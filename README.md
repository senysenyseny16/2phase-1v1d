## Goals :octopus:

- Verify canonical decomposition A = RΛL.
- Develop non-reflect boundary condition.
- Test scheme for various standard numerical tests.
- Create UI for experimenting with scheme (I plan to use ImGui to create nice interface/visualization).

