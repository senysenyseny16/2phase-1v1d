ifeq ($(OS),Windows_NT)
  RUN_EXT := .exe
else
  RUN_EXT :=
endif

CATCH_DIR ?= ./externals/catch2
EIGEN_DIR ?= ./externals/eigen

TARGET_NAME ?= Multiphase
TARGET_EXEC ?= $(TARGET_NAME)$(RUN_EXT)
TEST_EXEC   ?= Tests$(RUN_EXT)

BUILD_DIR ?= ./build
SRC_DIRS  ?= ./src
TEST_DIRS ?= ./tests

SRCS := $(shell find $(SRC_DIRS) -name *.cpp)
OBJS := $(SRCS:%=$(BUILD_DIR)/%.o)
ASMS := $(SRCS:%=$(BUILD_DIR)/%.s)
DEPS := $(OBJS:.o=.d)
SRCS_TEST := $(shell find $(TEST_DIRS) -name *.cpp)
OBJS_TEST := $(filter-out $(BUILD_DIR)/$(SRC_DIRS)/$(TARGET_NAME).cpp.o, $(OBJS)) $(SRCS_TEST:%=$(BUILD_DIR)/%.o)
DEPS_TEST := $(OBJS_TEST:.o=.d)

INC_DIRS  := $(shell find $(SRC_DIRS) -type d) $(EIGEN_DIR)
INC_FLAGS := $(addprefix -I,$(INC_DIRS))
INC_DIRS_TEST  := $(INC_DIRS) $(shell find $(TEST_DIRS) -type d) $(CATCH_DIR)
INC_FLAGS_TEST := $(addprefix -I,$(INC_DIRS_TEST))

CPPFLAGS ?= $(INC_FLAGS) -MMD -MP
CXXFLAGS ?= -Wall -O2 -std=c++17 -fopenmp
LDFLAGS  ?= -fopenmp

$(BUILD_DIR)/$(TARGET_EXEC): $(OBJS)
	@echo Linking solver \($@\)
	$(CXX) $(OBJS) -o $@ $(LDFLAGS)

$(BUILD_DIR)/%.cpp.o: %.cpp
	$(MKDIR_P) $(dir $@)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c $< -o $@

$(BUILD_DIR)/%.cpp.s: %.cpp
	$(MKDIR_P) $(dir $@)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -S $< -o $@

debug: CPPFLAGS += -DDEBUG -g
debug: CXXFLAGS += -DDEBUG -g
debug: $(BUILD_DIR)/$(TARGET_EXEC)

.PHONY: tests

tests: $(BUILD_DIR)/$(TEST_EXEC)

$(BUILD_DIR)/$(TEST_EXEC): $(OBJS_TEST)
	@echo Linking tests \($@\)
	$(CXX) $(OBJS_TEST) -o $@ $(LDFLAGS)

$(BUILD_DIR)/%.cpp.o: %.cpp
	$(MKDIR_P) $(dir $@)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) $(INC_FLAGS_TEST) -c $< -o $@

.PHONY: disasm

disasm: $(ASMS)

.PHONY: clean

clean:
	$(RM) -r $(BUILD_DIR)

-include $(DEPS)
-include $(DEPS_TEST)

MKDIR_P ?= mkdir -p
