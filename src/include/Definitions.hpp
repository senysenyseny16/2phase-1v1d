//: Definitions.hpp

#pragma once

#include <cstddef> // for size_t.
#include <algorithm> // for std::max.

//! Physical definitions.
namespace Physics {

  /// Number of variables in evolution field.
  const size_t varCount = 9;

  // Sound speed, reference density and pressure in porous media.
  // gamma1 - constant in liquid part of EOS.
  // mu     - constant in elastic part of EOS.
  const double C1     = 4.760;  // [cm / 10^-5 s]
  const double rho10  = 8.49;   // [g  / cm^3]
  const double p10    = 0.0001; // [g  / (cm * 10^-10 s)]
  const double gamma1 = 2.8;
  const double mu     = 98.;

  // Sound speed, reference density and pressure in liquid.
  // gamma2 - constant of EOS.
  const double C2     = 1.5;    // [cm / 10^-5 s]
  const double rho20  = 1.0;    // [g  / cm^3]
  const double p20    = 0.0001; // [g  / (cm * 10^-10 s)]
  const double gamma2 = 2.4;

  /// Area size.
  const double areaLength = 100.; // [cm]

  /// Time of physical process.
  const double timeEnd = 1e+5; // [10^(-5) s]

} // namespace Physics

//! Geometry parameters of mesh.
namespace Geometry {

  /// Number of cells.
  const size_t cellCount = 400;

  /// Number of ghost cells.
  const size_t cellCountGhost = 1;

  /// Index of first cell.
  const size_t firstCell = cellCountGhost;

  /// Index of last cell.
  const size_t lastCell = firstCell + cellCount - 1;

  /// Number of [normal + ghost] cells.
  const size_t meshSize = cellCount + 2 * cellCountGhost;

  enum class Edge { XB, XF };

} // namespace Geometry

//! Numerical scheme parameters.
namespace SchemePars {

  /// Size of cell.
  const double h = Physics::areaLength / Geometry::cellCount;

  /// CFL number.
  const double CFL = 0.3;

  /// Types of numerical flux.
  enum class FluxType {
      HLLC /// HLLC
  };

  /// Type of numerical flux used in scheme.
  const FluxType fluxType = FluxType::HLLC;

  /// Default value of frequency of data saving.
  const size_t defaultSaveInterval = 100;

  /// Default value of frequency of status output.
  const size_t defaultStatusInterval = 50;

  /// Weight of WENO-reconstruction.
  const double wenoEps = 1e-154;

  /// Alpha1 is bounded by interval (alpha1Eps, 1 - alpha1Eps).
  const double alpha1MinValue = 1e-14;

  /// Value for filling ghost cells.
  const double ghostCellsFillValue = 1e+7;

} // namespace SchemePars

//! Pressure relaxation parameters (relaxation step).
namespace PressureRelaxation {

  /// Enable or disable pressure relaxation.
  const bool enablePressureRelaxation = false;

  /// Accuracy of instant pressure relaxation.
  const double pressureRelaxationTol = 1e-14;

  /// Max number of interations with Newton method.
  const size_t maxIterationsNewton = 30;

  /// Max number of interations with Bisection method.
  const size_t maxIterationsBisection = 150;

} // namespace PressureRelaxation

//! THINC reconstruction parameters.
namespace THINC {

  /// Enable or disable THINC reconstruction.
  const bool enableTHINC = false;

  /// Smoothness of reconstruction.
  const double beta = 1.8;

  /// Minimum beta value (for weighted reconstruction).
  const double minBeta = 0.01;

  /// Interface indicator.
  const double eps = 9e-6;

  enum class RecType {
      Constant  /// No reconstruction (first order).
    , WenoThird /// WENO-3 (third order).
    , WenoFifth /// WENO-5 (fifth order).
    , Muscl2    /// MUSCL (second order).
  };

} // namespace THINC

namespace SlidingTreatment {

  const double eps = 0.0;

} // namespace SlidingTreatment

//! Boundary conditions.
namespace BC {

  /// Types of left boundary conditions.
  enum class BoundaryCondLeft {
      Periodic
  };

  /// Types of right boundary conditions.
  enum class BoundaryCondRight {
      Periodic
  };

  /// Left boundary condition.
  const BoundaryCondLeft boundaryCondLeft = BoundaryCondLeft::Periodic;
  /// Right boundary condition.
  const BoundaryCondRight boundaryCondRight = BoundaryCondRight::Periodic;

} // namespace BC

